// Commonly used Http methods and their actions
// METHOD	ACTION
// GET 		Retrieves resources
// POST		Sends data for creating a resource
// PUT		Sends data for updating a resource
// DELETE	Deletes a specified resource
// 
// Postman Client
// We'll use Postman Client to improve the experience of sending request,
// 